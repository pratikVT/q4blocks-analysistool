Program Analysis and Refactoring Tool for Blocks (Scratch)
==========================================================

# Set up
1. fork this repository
2. put jar dependencies from https://drive.google.com/drive/folders/1RQAIw2YJ3HY7NV5Bl2vOmTMoJC9ne8OL?usp=sharing into its corresponding directory (/tools and /libs)
3. `ant run`