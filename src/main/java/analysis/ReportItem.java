package analysis;

import java.util.HashSet;

public class ReportItem {
	private String scriptable;
	private HashSet<String> instances;

	public ReportItem(String name, HashSet<String> scripts) {
		this.scriptable = name;
		this.instances = scripts;
	}
}