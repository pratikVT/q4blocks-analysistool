package analysis.smell.cloneutils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import analysis.parsing.ScratchBlocksMapper;

public class FragmentLSH {
	int dimension = ScratchBlocksMapper.id2name.size();
	int bands = 2;
	int buckets = 20;
	LSHSuperBit lsh;
	Map<Integer, Set<Fragment>> cloneGroupBucket;

	public FragmentLSH() {
		lsh = new LSHSuperBit(bands, buckets, dimension);
		cloneGroupBucket = new HashMap<>();
	}

	public void hashFragments(List<Fragment> fragments) {
		int progress = 0;
		for (Fragment fragment : fragments) {
			double[] vector = fragment.toNameCounter().getVector();
			int[] hash = lsh.hash(vector);
			if (!cloneGroupBucket.containsKey(hash[0])) {
				cloneGroupBucket.put(hash[0], new HashSet<Fragment>());
			}
			cloneGroupBucket.get(hash[0]).add(fragment);
		}
	}

	public Map<Integer, Set<Fragment>> getBucketsOfCandidateClones() {
		return cloneGroupBucket;
	}

}
