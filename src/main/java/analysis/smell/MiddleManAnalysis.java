package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import analysis.Analysis;
import analysis.VisualizableAnalysis;
import analysis.ast.BroadcastBlock;
import analysis.ast.MessageBlock;
import analysis.ast.Scriptable;
import analysis.parsing.Util;

public class MiddleManAnalysis extends VisualizableAnalysis {

	public static final String SMELL_NAME = "Middle Man";
	public List<List<MessageBlock>> middleManPath;
	public Map<Scriptable, List<List<MessageBlock>>> middleManPathMap = new HashMap<>();

	@Override
	public void initialize() {
	}

	List<Number> chainLengthDist = new ArrayList<>();

	int count = 0;

	@Override
	public void performAnalysis() {
		middleManPath = new ArrayList<>();
		for (MessageBlock senderBlock : program.allMessageSenders()) {
			Set<MessageBlock> reachableReceiverBlocks = senderBlock.reachable();
			for (MessageBlock receiverBlock : reachableReceiverBlocks) {
				List<MessageBlock> path = BFS(senderBlock, receiverBlock);
				if (hasMiddleManSmell(path)) {
					chainLengthDist.add(path.size() / 2);

					middleManPath.add(path);
					Scriptable spriteOrigin = path.get(0).spriteParent();
					if (!middleManPathMap.containsKey(spriteOrigin)) {
						middleManPathMap.put(spriteOrigin, new ArrayList<List<MessageBlock>>());
					}
					middleManPathMap.get(spriteOrigin).add(path);
					count++;
				}
			}
		}
	}

	private boolean hasMiddleManSmell(List<MessageBlock> path) {
		boolean isSmell = false;
		// only greater than 2 that MM smell can occur
		// if sender in the middle located in the script with just a
		// receiver and the sender itself
		if (path.size() > 2) {
			// assume that all script sender/receiver in the
			// middle has only two blocks
			isSmell = true;
			for (int i = 1; i < path.size() - 1; i++) {
				int loc = path.get(i).parentScript().getLineOfCode();
				// need just one script loc > 2 to make smell invalid
				if (loc != 2) {
					isSmell = false;
				}
			}
		}
		return isSmell;
	}

	List<MessageBlock> BFS(MessageBlock sender, MessageBlock targetSearch) {
		Map<MessageBlock, Boolean> visited = new HashMap<>();

		// Create a queue for BFS
		LinkedList<MessageBlock> queue = new LinkedList<MessageBlock>();
		LinkedList<List<MessageBlock>> pathQueue = new LinkedList<>();

		// setup
		// Mark the current node as visited and enqueue it
		visited.put(sender, true);
		queue.add(sender);
		List<MessageBlock> firstPath = new ArrayList<MessageBlock>();
		firstPath.add(sender);
		pathQueue.add(firstPath);

		while (queue.size() != 0) {
			// Dequeue a vertex from queue and print it
			sender = queue.poll();
			List<MessageBlock> path = pathQueue.poll();
			sender = path.get(path.size() - 1);

			Iterator<MessageBlock> receiverIter = sender.getReceivers().iterator();
			while (receiverIter.hasNext()) {
				MessageBlock receiver = receiverIter.next();
				// also add broadcast within same script to the queue
				HashSet<BroadcastBlock> messageSenders = receiver.parentScript().messageSenders();
				path.add(receiver);
				if (receiver.equals(targetSearch)) {
					return path;
				}

				queue.addAll(messageSenders);
				for (BroadcastBlock senderBlock : messageSenders) {
					ArrayList<MessageBlock> newPath = new ArrayList<>(path);
					newPath.add(senderBlock);
					pathQueue.add(newPath);
				}
				// fix : messageSenders should output list to preserve ordering
			}
			visited.put(sender, true);
		}
		return new ArrayList<>();
	}

	@Override
	protected void buildFullReport() {

		fullReport.put("name", SMELL_NAME);
		List<ReportItem> instances = new ArrayList<ReportItem>();
		Set<EdgeObj> edgeInfos = new HashSet<EdgeObj>();
		for (Scriptable scriptable : middleManPathMap.keySet()) {
			List<List<MessageBlock>> paths = middleManPathMap.get(scriptable);
			for (List<MessageBlock> path : paths) {
				Set<InstanceItem> msgChainElement = new LinkedHashSet<>();
				MessageBlock previous = null;
				for (MessageBlock msgBlock : path) {
					msgChainElement.add(new InstanceItem(msgBlock.spriteParent().getName(),
							msgBlock.spriteParent().getCostume(0).getValue(), msgBlock.parentScript().render()));
					// add edge info (when broadcast -> receive but not receive -> broadcast)
					if (previous != null) {
						edgeInfos.add(
								new EdgeObj(previous.spriteParent(), msgBlock.spriteParent(), previous.getMessage()));
					}
					previous = msgBlock;
				}

				ReportItem item = new ReportItem(msgChainElement);
				instances.add(item);
			}
		}

		// visualization
		initializeVisNetwork();
		for (ReportItem i : instances) {
			InstanceItem prevElement = null;
			for (InstanceItem chainElement : i.instance) {
				// make node
				getVisNetwork().makeNode(chainElement.scriptable).addProperty("image", chainElement.costume);
			}
		}

		for (EdgeObj eObj : edgeInfos) {
			// make edge
			getVisNetwork().makeEdge(eObj.from.getName(), eObj.to.getName()).setLabel(eObj.msg);
		}

		// previous = msgBlock;

		fullReport.put("instances", instances);
	}

	public class EdgeObj {
		Scriptable from;
		Scriptable to;
		String msg;

		public EdgeObj(Scriptable from, Scriptable to, String message) {
			this.from = from;
			this.to = to;
			this.msg = message;
		}
		
		@Override
		public int hashCode() {
			return from.hashCode() + to.hashCode() + msg.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			if (o == null) {
				return false;
			}
			if (this == o) {
				return true;
			}

			if (this.getClass() != o.getClass()) {
				return false;
			}
			final EdgeObj other = (EdgeObj) o;
			
			if(this.from != other.from || this.to !=other.to ||!this.msg.equals(other.msg)) {
				return false;
			}
			
			return true;
		}
	}

	public class ReportItem {
		public List<InstanceItem> instance;

		public ReportItem() {
		}

		public ReportItem(Set<InstanceItem> msgChainElements) {
			instance = new ArrayList<>(msgChainElements);
		}
	}

	class InstanceItem {
		String scriptable;
		String location;
		String costume;

		public InstanceItem(String scriptable, String costume, String location) {
			this.scriptable = scriptable;
			this.costume = costume;
			this.location = location;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(this instanceof InstanceItem))
				return false;
			InstanceItem that = (InstanceItem) o;
			return this.location.equals(that.location);
		}

		@Override
		public int hashCode() {
			int result = 17;
			result = 37 * result + scriptable.hashCode();
			result = 37 * result + location.hashCode();
			return result;
		}

	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "MM");
		shortReport.put("count", count);
		shortReport.put("chain_length_dist", Util.toWeighedValues(chainLengthDist));
	}

}
