package analysis.smell.visualize;

import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

public class NodeAdapter implements JsonSerializer<Node> {

	@Override
	public JsonElement serialize(Node node, Type type, JsonSerializationContext ctx) {
		JsonObject obj = new JsonObject();
		obj.addProperty("id", node.id);
		obj.addProperty("label", node.label);
		
		
		Gson gson = new Gson();
		if(node.properties.containsKey("snippets")) {
        	JsonElement snippets = gson.toJsonTree(node.properties.get("snippets"), 
        			new TypeToken<Collection<String>>() {}.getType());   
        	obj.add("snippets", snippets);
        }
		
		if (node.properties.containsKey("image")) {
			obj.addProperty("shape", "image");
			String url = String.format("https://cdn.assets.scratch.mit.edu/internalapi/asset/%s/get/",
					node.properties.get("image"));
			obj.addProperty("image", url);
		}
		return obj;
	}

}
