package analysis.smell.visualize;

import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

public class NetworkAdapter implements JsonSerializer<Network>{

	@Override
	public JsonElement serialize(Network nw, Type type, JsonSerializationContext ctx) {
		JsonObject obj = new JsonObject();
		Gson gson = Network.getGson();
		JsonElement nodes = gson.toJsonTree(nw.nodeMap.values(), new TypeToken<Collection<Node>>() {}.getType());
		JsonElement edges = gson.toJsonTree(nw.edges, new TypeToken<Collection<Edge>>() {}.getType());
		obj.add("nodes", nodes);
		obj.add("edges", edges);
        return obj;
	}
	
}
