package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import analysis.Analysis;
import analysis.ReportItem;
import analysis.ast.ScratchBlock;
import analysis.ast.Scriptable;
import analysis.ast.Sprite;
import analysis.ast.UndefinedBlock;
import analysis.ast.VarDecl;

public class UndefinedBlockAnalysis extends Analysis {


	private Map<Scriptable, List<UndefinedBlock>> resultMap;

	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}
	
	int count = 0;
	
	@Override
	public void performAnalysis() {
		HashSet<UndefinedBlock> allUndefinedBlocks = program
				.allUndefinedBlocks();
		resultMap = new HashMap<>();
		Iterator<UndefinedBlock> blockIter = allUndefinedBlocks.iterator();
		while (blockIter.hasNext()) {
			UndefinedBlock block = blockIter.next();
			if(!resultMap.containsKey(block.spriteParent())){
				resultMap.put(block.spriteParent(),
						new ArrayList<UndefinedBlock>());
			}
			resultMap.get(block.spriteParent()).add(block);
			count++;
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "Undefined Block");
		List<ReportItem> instances = new ArrayList<ReportItem>();

		for (Scriptable scriptable : resultMap.keySet()) {
			HashSet<String> undefinedBlockStr = new HashSet<>();
			for (UndefinedBlock block : resultMap.get(scriptable)) {
				undefinedBlockStr.add(block.render());
			}
			instances.add(new ReportItem(scriptable.getName(), undefinedBlockStr));
		}
		fullReport.put("scriptables", instances);
	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "UB");
		shortReport.put("count", count);

	}

}
