package analysis.parsing.scratch3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import analysis.ast.BlockExp;
import analysis.ast.BlockSeq;
import analysis.ast.CommandBlock;
import analysis.ast.DoRepeatBlock;
import analysis.ast.Expression;
import analysis.ast.NumLiteral;
import analysis.ast.Program;
import analysis.ast.ScratchBlock;
import analysis.ast.Script;
import analysis.ast.Sprite;
import analysis.ast.Stage;
import analysis.parsing.ScratchBlocksMapper;

public class Scratch3ASTBuilder {
	JsonParser jsonParser = new JsonParser();
	JsonObject currBlocks = null;
	HashSet<String> currParsedBlockIDs = null;

	public Scratch3ASTBuilder() {
	};

	public List<Script> parseBlockList(JsonObject blockList) {
		List<Script> scriptList = new ArrayList<Script>();
		currBlocks = blockList;

		Script currScript = null;

		List<String> topBlockIDs = new ArrayList<>();
		// first pass to look for top-level block (script) to eliminate keeping track of
		// seen block
		for (Entry<String, JsonElement> blockEntry : blockList.entrySet()) {
			JsonObject currJSONBlock = blockEntry.getValue().getAsJsonObject();
			boolean isTopLevel = currJSONBlock.get("topLevel").getAsBoolean();
			boolean isShadow = currJSONBlock.get("shadow").getAsBoolean();

			if (isTopLevel && !isShadow) { // and is not shadow (a shadow block can be top-level)
				topBlockIDs.add(currJSONBlock.get("id").getAsString());
			}
		}

		for (String id : topBlockIDs) {
			JsonObject currJSONBlock = blockList.get(id).getAsJsonObject();

			boolean isTopLevel = currJSONBlock.get("topLevel").getAsBoolean();
			if (isTopLevel) {
				currScript = new Script();
				currScript.setBody(new BlockSeq());
				scriptList.add(currScript);
			}

			if (currJSONBlock.get("shadow").getAsBoolean()) {
				continue;
			}

			BlockSeq blockSeq = parseBlockSeq(id, blockList);

			currScript.setBody(blockSeq);

		}

		return scriptList;
	}

	private BlockSeq parseBlockSeq(String topBlockID, JsonObject blockList) {
		BlockSeq blockSeq = new BlockSeq();
		String blockID = topBlockID;
		do {
			JsonObject currJSONBlock = blockList.get(blockID).getAsJsonObject();
			ScratchBlock block = parseBlock(currJSONBlock, blockList);
			blockSeq.addBlocks(block);
			blockID = nextBlockID(currJSONBlock);
		} while (blockID != null);

		return blockSeq;
	}

	private String nextBlockID(JsonObject jsonBlock) {
		JsonElement next = jsonBlock.get("next");
		if (next.isJsonNull()) {
			return null;
		}
		return next.getAsString();
	}

	public ScratchBlock buildBlock(String mainBlockID, JsonObject jsonBlockMap) {
		JsonObject jsonBlock = jsonBlockMap.get(mainBlockID).getAsJsonObject();
		ScratchBlock currBlock = parseBlock(jsonBlock, jsonBlockMap);
		return currBlock;
	}

	public ScratchBlock parseBlock(JsonObject mainJsonBlock, JsonObject jsonBlockMap) {
		String opcode = mainJsonBlock.get("opcode").getAsString();
		ScratchBlock currBlock = null;
		if (opcode.contains("math")) {
			currBlock = parseNumLiteral(mainJsonBlock, jsonBlockMap);
		} else if (opcode.equals("control_repeat")) {
			currBlock = parseRepeatBlock(mainJsonBlock, jsonBlockMap);
		} else if (opcode.contains("operator")) {
			currBlock = parseExpressionBlock(mainJsonBlock, jsonBlockMap);
		} else {
			currBlock = parseCommandBlock(mainJsonBlock, jsonBlockMap);
		}

		return currBlock;
	}

	private ScratchBlock parseExpressionBlock(JsonObject mainJsonBlock, JsonObject jsonBlockMap) {
		BlockExp block = null;
		String opcode = mainJsonBlock.get("opcode").getAsString();
		String sb2Name = ScratchBlocksMapper.sb3tosb2.get(opcode);
		if (opcode.contains("operator")) {
			block = new BlockExp();
		}
		block.setOperator(sb2Name);
		// lookup specification to handle the inputs
		JsonObject inputEntries = mainJsonBlock.get("inputs").getAsJsonObject();
		for (Entry<String, JsonElement> inputEntry : inputEntries.entrySet()) {
			String inputBlockID = inputEntry.getValue().getAsJsonObject().get("block").getAsString();
			Expression inputBlock = null;
			try {
				inputBlock = (Expression) buildBlock(inputBlockID, jsonBlockMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			block.addOperand(inputBlock);
		}

		return block;
	}

	private ScratchBlock parseCommandBlock(JsonObject jsonBlock, JsonObject jsonBlockMap) {
		CommandBlock block = new CommandBlock();
		String opcode = jsonBlock.get("opcode").getAsString();
		String sb2Name = ScratchBlocksMapper.sb3tosb2.get(opcode);
		block.setBlockName(sb2Name);

		JsonObject inputEntries = jsonBlock.get("inputs").getAsJsonObject();
		for (Entry<String, JsonElement> inputEntry : inputEntries.entrySet()) {
			String inputBlockID = inputEntry.getValue().getAsJsonObject().get("block").getAsString();
			Expression inputBlock = null;
			try {
				inputBlock = (Expression) buildBlock(inputBlockID, jsonBlockMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			block.addArgument(inputBlock);
		}

		return block;
	}

	public ScratchBlock buildInputBlock(String name, String blockID, JsonObject jsonBlockMap) {
		if (name.equals("SUBSTACK")) {
			BlockSeq blockSeq = new BlockSeq();
			String currBlockID = blockID;
			while (blockID != null) {
				JsonObject jsonBlock = jsonBlockMap.get(currBlockID).getAsJsonObject();
				ScratchBlock block = buildBlock(currBlockID, jsonBlockMap);
				blockSeq.addBlocks(block);
				blockID = nextBlockID(jsonBlock);
			}
			return blockSeq;
		}

		return buildBlock(blockID, jsonBlockMap);
	}

	private ScratchBlock parseRepeatBlock(JsonObject jsonBlock, JsonObject jsonBlockMap) {
		DoRepeatBlock block = new DoRepeatBlock();
		JsonObject inputEntries = jsonBlock.get("inputs").getAsJsonObject();
		for (Entry<String, JsonElement> inputEntry : inputEntries.entrySet()) {
			String inputBlockName = inputEntry.getValue().getAsJsonObject().get("name").getAsString();
			String inputBlockID = inputEntry.getValue().getAsJsonObject().get("block").getAsString();

			ScratchBlock inputBlock = null;

			if (inputBlockName.equals("TIMES")) {
				inputBlock = buildBlock(inputBlockID, jsonBlockMap);
				block.setRepNum((NumLiteral) inputBlock);
				continue;
			}

			if (inputBlockName.equals("SUBSTACK")) {
				inputBlock = parseBlockSeq(inputBlockID, jsonBlockMap);
				block.setBody((BlockSeq) inputBlock);
			}
		}

		return block;
	}

	private ScratchBlock parseNumLiteral(JsonObject jsonBlock, JsonObject jsonBlockMap) {
		NumLiteral block = new NumLiteral();
		String strVal = jsonBlock.get("fields").getAsJsonObject().get("NUM").getAsJsonObject().get("value")
				.getAsString();
		Number value = Double.parseDouble(strVal);
		block.setValue(value);
		return block;
	}

	public Program parseProgram(JsonObject jsonElement) {
		Program program = new Program();
		program.setStage(new Stage());

		JsonArray targets = jsonElement.get("targets").getAsJsonArray();
		for (JsonElement jsonTarget : targets) {
			JsonObject obj = jsonTarget.getAsJsonObject();
			String scriptableName = obj.get("name").getAsString();
			if (scriptableName.equals("Stage") && obj.get("isStage").getAsBoolean()) {
				parseStage(program, obj);
			} else {
				Sprite sprite = parseSprite(obj);
				program.getStage().addSprite(sprite);
			}
		}

		return program;
	}

	void parseStage(Program program, JsonObject obj) {
		Stage stage = program.getStage();
	}

	Sprite parseSprite(JsonObject obj) {
		String scriptableName = obj.get("name").getAsString();
		JsonObject blockMap = obj.get("blocks").getAsJsonObject();
		Sprite sprite = new Sprite();
		sprite.setName(scriptableName);
		List<Script> scripts = parseBlockList(blockMap);
		for (Script script : scripts) {
			sprite.addScript(script);
			System.out.println(script.render());
		}
		return sprite;
	}

}
