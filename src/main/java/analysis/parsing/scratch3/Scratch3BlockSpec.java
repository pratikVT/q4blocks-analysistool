package analysis.parsing.scratch3;

import java.util.List;

public class Scratch3BlockSpec {
	String blockName;
	String opcode;
	List<ArgSpec> args;
	
	public class ArgSpec {
		String type;
		String inputOp;
		String inputName;
	}
}
