package analysis.parsing.scratch3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import analysis.ast.BlockExp;
import analysis.ast.BlockSeq;
import analysis.ast.CommandBlock;
import analysis.ast.DoIfBlock;
import analysis.ast.DoIfElseBlock;
import analysis.ast.DoRepeatBlock;
import analysis.ast.Expression;
import analysis.ast.Literal;
import analysis.ast.NumLiteral;
import analysis.ast.ScratchBlock;
import analysis.ast.Script;
import analysis.ast.Scriptable;
import analysis.ast.Sprite;
import analysis.ast.StringLiteral;
import analysis.ast.VarAccess;
import analysis.ast.VarDecl;
import analysis.ast.VariableExpBlock;
import analysis.parsing.ScratchBlocksMapper;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;
import nu.xom.ParsingException;
import nu.xom.Text;
import nu.xom.ValidityException;

public class Scratch3XMLParser {

	private static final String NAMESPACE = "http://www.w3.org/1999/xhtml";

	public static List<Script> parse(String xml) throws Exception {
		List<Script> scripts = new ArrayList<Script>();
		Builder parser = new Builder();
		Document doc = parser.build(xml, null);
		Element rootEl = doc.getRootElement();
		Elements childElements = rootEl.getChildElements();
		for (int i = 0; i < childElements.size(); i++) {
			Element curEl = childElements.get(i);

			if (curEl.getLocalName().equals("variables")) {
				continue;
			}

			if (curEl.getLocalName().equals("shadow")) {
				continue;
			}

			Script script = parseScript(curEl);
			scripts.add(script);
		}
		return scripts;
	}

	private static Script parseScript(Element blockEl) throws Exception {
		// call parse block as long as there is next
		Script script = new Script();
		BlockSeq blockSeq = new BlockSeq();
		script.setBody(blockSeq);
		parseBlocks(blockEl, blockSeq);
		return script;
	}

	private static void parseBlocks(Element blockEl, BlockSeq blockSeq) throws Exception {
		Element currEl = blockEl;
		do {
			ScratchBlock block = parseBlock(currEl);
			blockSeq.addBlocks(block);
			Element nextEl = currEl.getFirstChildElement("next");
			if (nextEl != null) {
				currEl = nextEl.getFirstChildElement("block");
			} else {
				currEl = null;
			}
		} while (currEl != null);
	}

	private static ScratchBlock parseBlock(Element blockEl) throws Exception {
		String sb3Opcode = blockEl.getAttributeValue("type");
		String sbName = ScratchBlocksMapper.sb3tosb2.get(sb3Opcode); // TODO: MAKE IT FAST

		// ScratchBlock
		// TODO: decide if it's expression?

		if (sb3Opcode.contains("operator")) {
			return parseExpBlock(blockEl);
		} else if (sb3Opcode.equals("control_repeat")) {
			return parseRepeatBlock(blockEl);
		} else if (sb3Opcode.equals("control_if")) {
			return parseDoIfBlock(blockEl);
		} else if (sb3Opcode.equals("control_if_else")) {
			return parseDoIfElseBlock(blockEl);
		} else {
			return parseCommandBlock(blockEl);
		}
	}

	private static ScratchBlock parseDoIfBlock(Element blockEl) throws Exception {
		DoIfBlock block = new DoIfBlock();

		if (blockEl.getFirstChildElement("value").getAttributeValue("name").equals("CONDITION")) {
			block.setCondition(parseExpBlock(blockEl.getFirstChildElement("value").getFirstChildElement("block")));
		}

		if (blockEl.getFirstChildElement("statement").getAttributeValue("name").equals("SUBSTACK")) {
			Element blockSeqEl = blockEl.getFirstChildElement("statement").getFirstChildElement("block");
			BlockSeq blockSeq = new BlockSeq();
			parseBlocks(blockSeqEl, blockSeq);
			block.setConsequent(blockSeq);
		}

		return block;
	}

	private static ScratchBlock parseDoIfElseBlock(Element blockEl) throws Exception {
		DoIfElseBlock block = new DoIfElseBlock();

		if (blockEl.getFirstChildElement("value").getAttributeValue("name").equals("CONDITION")) {
			block.setCondition(parseExpBlock(blockEl.getFirstChildElement("value").getFirstChildElement("block")));
		}

		Elements elements = blockEl.getChildElements();

		Element blockSeqEl = elements.get(1).getFirstChildElement("block");
		BlockSeq blockSeq = new BlockSeq();
		parseBlocks(blockSeqEl, blockSeq);
		block.setConsequent(blockSeq);

		Element blockSeqEl2 = elements.get(2).getFirstChildElement("block");
		BlockSeq blockSeq2 = new BlockSeq();
		parseBlocks(blockSeqEl, blockSeq);
		block.setAlternate(blockSeq);

		return block;
	}

	private static ScratchBlock parseRepeatBlock(Element blockEl) throws Exception {
		DoRepeatBlock block = new DoRepeatBlock();

		if (blockEl.getFirstChildElement("value").getAttributeValue("name").equals("TIMES")) {
			block.setRepNum(parseInputValue(blockEl.getFirstChildElement("value")));
		}

		if (blockEl.getFirstChildElement("statement").getAttributeValue("name").equals("SUBSTACK")) {
			Element blockSeqEl = blockEl.getFirstChildElement("statement").getFirstChildElement("block");
			BlockSeq blockSeq = new BlockSeq();
			parseBlocks(blockSeqEl, blockSeq);
			block.setBody(blockSeq);
		}

		return block;
	}

	private static ScratchBlock parseCommandBlock(Element blockEl) throws Exception {
		String sb3Opcode = blockEl.getAttributeValue("type");
		String blockId = blockEl.getAttributeValue("id");
		String sbName = ScratchBlocksMapper.sb3tosb2.get(sb3Opcode); // TODO: MAKE IT FAST
		CommandBlock block = new CommandBlock();
		block.setBlockName(sbName);
		block.setBlockID(blockId);

		// iterate input listing
		// if a block is an input (there could be  elements (shadow and block)
		// TODO: shadow value as block attributes (list of primitive values)

		Elements values = blockEl.getChildElements("value", null);

		for (int i = 0; i < values.size(); i++) {
			Element valueEl = values.get(i);
			Expression inputBlock = parseInputValue(valueEl);
			block.addArgument(inputBlock);

		}
		return block;

	}

	private static Expression parseInputValue(Element valueEl) throws Exception {
		// a value has at least a shadow block (default primitive)
		// and additionally block
		Element element = null;
		if (valueEl.getFirstChildElement("block") != null) {
			return parseExpression(valueEl.getFirstChildElement("block"));
		}
		// TODO: add shadow as attribute of block?
		// TODO: lookahead to see what type of input num or str
		// valueEl->shadow.type = "math_number"?
		if (valueEl.getFirstChildElement("shadow") != null) {
			return parseNumLiteral(valueEl.getFirstChildElement("shadow"));
		}

		return null;
	}

	public static Expression parseExpression(Element expEl) throws Exception {

		// when expression block is a stand-alone (fragment block, not runnable)
		if (expEl.getLocalName().equals("block")) {
			return parseExpBlock(expEl);
		}

		if (expEl.getLocalName().equals("shadow")) {
			return parseLiteral(expEl);
		}

		throw new Exception("Unrecognized Expression");

	}

	private static Expression parseExpBlock(Element expBlockEl) throws Exception {
		String sb3Opcode = expBlockEl.getAttributeValue("type");
		String sbName = ScratchBlocksMapper.sb3tosb2.get(sb3Opcode); // TODO: MAKE IT FAST
		
		String ID = expBlockEl.getAttributeValue("id");
		Expression expBlock = null;

		if (sb3Opcode.contains("operator")) {
			expBlock = new BlockExp();
			((BlockExp) expBlock).setOperator(sbName);
			
			// input args?
			Elements valueElements = expBlockEl.getChildElements();
			for (int i = 0; i < valueElements.size(); i++) {
				Element valueEl = valueElements.get(i);
				Expression operand = parseInputValue(valueEl);
				((BlockExp) expBlock).addOperand(operand);
			}
		
		}else if (sb3Opcode.contains("data")) {
			expBlock = new VariableExpBlock();
			VarAccess v = new VarAccess();
			// parsing field
			Element fieldEl = expBlockEl.getFirstChildElement("field");
			v.setID(fieldEl.getAttributeValue("id"));
			v.setName(fieldEl.getValue());
			((VariableExpBlock)expBlock).setVar(v);
		}
		
		expBlock.setBlockID(ID);
		

		return expBlock;
	}

	private static Expression parseNumLiteral(Element valueEl) {
		NumLiteral block = new NumLiteral();
		Element fieldEl = valueEl.getFirstChildElement("field");
		String blockID = valueEl.getAttributeValue("id");
		block.setBlockID(blockID);
		String strVal = fieldEl.getValue();
		Number value;
		if (strVal.equals("")) {
			value = 0; // TODO: fix render to accept null
		} else {
			value = Double.parseDouble(strVal);
		}
		block.setValue(value);
		return block;
	}

	private static Literal parseLiteral(Element valueEl) {
		// handle string literal for now
		Literal block = new StringLiteral();
		Element fieldEl = valueEl.getFirstChildElement("field");
		String strVal = fieldEl.getValue();
		((StringLiteral) block).setValue(strVal);
		return block;
	}

	public static Scriptable parseBlocks(String xml) {
		// if stage return stage

		// if sprite
		Sprite sprite = new Sprite();
		return sprite;
	}

	public static List<VarDecl> parseVariables(Node node) {
		List<VarDecl> varList = new ArrayList<VarDecl>();
		for (int i = 0; i < node.getChildCount(); i++) {
			Node childNode = node.getChild(i);
			if (childNode instanceof Text) {
				continue;
			}
			VarDecl var = new VarDecl();
			var.setName(childNode.getValue());
			Element varElement = (Element) childNode;
			var.setID(varElement.getAttributeValue("id"));

			// No value yet: var.setValue(varElement.getAttributeValue("id"));
			varList.add(var);
		}

		return varList;
	}

	public static Expression parseExpressionFromXML(String xml) throws Exception {
		Builder parser = new Builder();
		Document doc = parser.build(xml, null);
		Element rootEl = doc.getRootElement();
		return parseExpression(rootEl);
	}

	public static ScratchBlock parseBlockFromXML(String blockXML) throws Exception {
		Builder parser = new Builder();
		Document doc = parser.build(blockXML, null);
		Element rootEl = doc.getRootElement();
		return parseBlock(rootEl);
		
	}

	public static Sprite parseScriptable(String xml) throws Exception {
		Sprite sprite = new Sprite();
		List<Script> scripts = parse(xml);
		for(Script s : scripts) {
			sprite.addScript(s);
		}
		return sprite;
	}

}
