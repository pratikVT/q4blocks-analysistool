package analysis;

import com.google.gson.JsonObject;

import analysis.smell.visualize.Network;

public abstract class VisualizableAnalysis extends Analysis {
	Network visNetwork = null;
	
	public void initializeVisNetwork() {
		visNetwork = new Network();
	}
	
	public Network getVisNetwork() {
		return visNetwork;
	}
	
	public String getVisualizableReport() {
		return visNetwork.getNetworkAsJSONString();
	}

	public JsonObject getVisualizableReportAsJSON() {
		return visNetwork.getNetworkAsJSONObject();
	}
	
	protected void buildFullReport() {
		initializeVisNetwork();
	}
}
