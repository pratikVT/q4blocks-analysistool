package analysis;

import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;
import analysis.ast.BlockSeq;
import analysis.ast.CommandBlock;
import analysis.ast.Program;
import analysis.ast.ScratchBlock;
import analysis.ast.Script;
import analysis.ast.Sprite;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;
import analysis.smell.cloneutils.Fragment;
import analysis.smell.cloneutils.NameCounter;

public class VectorGenerationTest extends TestCase {
	private final static String FILENAME = "/smell/test-VectorGeneration.json";
	Program program = null;

	@Override
	protected void setUp() {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		program = null;

		try {
			srd = Util.getStringReaderFromFile(FILENAME);
			program = parser.parse(srd);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void testVectorGeneration() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		NameCounter vec = script.getBody().getBlocks(0).toNameCounter();
		assertEquals(1, vec.nodeCount);
	}

	public void testVectorOfSubTree() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		ScratchBlock block = script.getBody().getBlocks(0);
	}

	public void testSlidingWindow() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		BlockSeq block = script.getBody();
		List<Fragment> sizeOnefragments = Fragment.getFragment(block, 1);
		assertEquals(3, sizeOnefragments.size());

		List<Fragment> sizeTwofragments = Fragment.getFragment(block, 2);
		assertEquals(4, sizeTwofragments.size());

		List<Fragment> sizeThreefragments = Fragment.getFragment(block, 3);
		assertEquals(3, sizeThreefragments.size());
	}

	public void testVectorOfFragment() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		BlockSeq blockSeq = script.getBody();
		List<Fragment> sizeOnefragments = Fragment.getFragment(blockSeq, 1);
		Fragment fragment = sizeOnefragments.get(0);
	}
}