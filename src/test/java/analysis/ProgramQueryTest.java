package analysis;

import java.io.StringReader;

import junit.framework.TestCase;
import analysis.ast.Program;
import analysis.ast.Scriptable;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;

public class ProgramQueryTest extends TestCase {
	private final static String FILENAME = "/test-input.txt";
	private final static int projectID = 152030022;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
	    	  srd = Util.retrieveProjectOnline(projectID);
//	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		Scriptable stage = program.lookupScriptable("_stage_");
		System.out.println(stage);
		Scriptable sprite = program.lookupScriptable("Sprite1");
		System.out.println(sprite);
		
	}
}